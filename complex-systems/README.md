# Introduction to the Modeling and Analysis of Complex Systems

Binghamton Professor, [Hiroki Sayama](http://bingweb.binghamton.edu/~sayama/)'s book ["Introduction to the Modeling and Analysis of Complex Systems"](http://textbooks.opensuny.org/introduction-to-the-modeling-and-analysis-of-complex-systems/) is the first Binghamton University publication to be added to Open SUNY. 

Sample python code retrieved March 25, 2019 from [Archive (.zip) of Python sample codes (updated on 9/9/2016)](http://bingweb.binghamton.edu/~sayama/textbook/code.zip)
