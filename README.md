# watson-advisory

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/alexanderfield%2Fwatson-advisory/master)

## Quickstart (changes won't be saved)

Click the Binder badge to launch an interactive notebook server.

Navigate to & launch the courses notebook.

## Easy local setup from scratch (pull requests welcome!)

1. Download & install Anaconda: 
https://www.anaconda.com/distribution/

2. Run these three commands:

   `$ conda env create --file=environment.yaml`

   `$ conda soure activate watson`

   `$ jupyter notebook`